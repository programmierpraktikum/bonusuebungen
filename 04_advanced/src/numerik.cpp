#include "numerik.hpp"
#include <cstddef>
#include <iostream>

double newton(std::function<double (double)> fx, std::function<float (float)> fderiv, double x0, size_t n) {
    // TODO
    double xn = x0;
    for (int i = 0; i < n; ++i) {
        xn = xn - (fx(xn) / fderiv(xn));
    }
    std::cout << xn << std::endl;
    return xn;
}
