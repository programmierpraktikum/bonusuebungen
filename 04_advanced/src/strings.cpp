#include "strings.hpp"
#include <algorithm>
#include <cctype>
#include <iterator>
#include <iostream>

size_t countAbc(const std::string& input) {
    // TODO
    int result = std::count_if(input.begin(), input.end(), [](char character){
        return (character >= 'A' and character <= 'Z') or (character >= 'a' and character <= 'z');
    });
    std::cout << result << std::endl;
    return result;
}

std::map<char, size_t> countIndividual(const std::string& input) {
    // TODO
    std::map<char, size_t> result = {};

    std::for_each(input.begin(), input.end(), [&](auto character){
        character = std::tolower(character);
         if(result.find(character)->first != character) {
            result.insert({character, 1});
         } else {
             result.find(character)->second = result.find(character)->second + 1;
         }
    });

    for (int i = 0; i < input.length(); ++i) {
        std::cout << result.find(std::tolower(input[i]))->first << ": " << result.find(std::tolower(input[i]))->second << std::endl;
    }
    return result;


}

SymbolCounter::SymbolCounter(std::initializer_list<char> lst) /* TODO */ {
    list = lst;
}

bool SymbolCounter::operator()(char c) const {
    // TODO
    bool result = false;
    std::for_each(list.begin(), list.end(), [&](auto character){
        if(character == c)
            result = true;
    });
    return result;
}

std::list<char> usedSymbols(const std::string& input) {
    // TODO
    std::string copy = input;
    for (int i = 0; i < copy.length(); ++i) {
        copy[i] = (copy[i] >= 'A' and copy[i] <= 'Z') ? std::tolower(copy[i]):copy[i];
    }

    std::sort(copy.begin(), copy.end(), [](auto a, auto b){
        return a < b; });
    auto last = std::unique(copy.begin(), copy.end());
    copy.erase(last, copy.end());

    std::for_each(copy.begin(), copy.end(), [](auto character){
        std::cout << character << std::endl;
    });

    std::list<char> result = {};
    result.assign(copy.begin(),copy.end());
    return result;
}
