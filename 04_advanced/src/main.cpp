#include <cctype>
#include <cstddef>
#include "strings.hpp"
#include "numerik.hpp"
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <ostream>
#include <string>
#include <algorithm>
#include <cmath>

int main(int argc, char** argv) {
    // TODO teste deine Implementationen
    //std::cout << usedSymbols(str).size();
   // std::for_each(usedSymbols(str).begin(), usedSymbols(str).end(), [](auto character) {
        //std::cout << character << std::endl;
    //});
    //4.1a) Test countAbc
    std::cout << "4.1a) Test countAbc:" << std::endl;
    countAbc("&abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ!");
    std::cout << std::endl;

    //4.1b) Test countIndividual
    std::cout << "4.1b) Test countIndividual:" << std::endl;
    countIndividual("Halloh&");
    std::cout << std::endl;

    //4.1c) Test SymbolCounter and (...) overload
    std::cout << "4.1c) Test SymbolCounter and (...) overload:" << std::endl;
    SymbolCounter sc({'A', '$', 'H', 'l'});
    std::string str = "Hallo$";
    size_t anzahl = std::count_if(str.begin(), str.end(), sc);
    std::cout << anzahl << std::endl << std::endl;

    //4.1c) Test usedSymbols
    std::cout << "4.1c) Test usedSymbols:" << std::endl;
    std::string string = "Hallo$";
    usedSymbols(string);
    std::cout << std::endl;

    //4.2a) Test newton
    std::cout << "4.2a) Test newton:" << std::endl;
    auto fx = [](double x){return std::pow(x, 3) + 4 * std::pow(x, 2) - 7 * x + 12;};
    auto fderiv = [](float x){return 3 * std::pow(x, 2) + 8 * x - 7;};
    newton(fx, fderiv, 0, 1000);
    std::cout << std::endl;

    //4.2b) Test newtonTemp with float, double, unsigned int and int
    std::cout << "4.2b) Test newtonTemp with float, double, unsigned int and int:" << std::endl;
    std::cout << "Test newtonTemp with float:" << std::endl;
    newtonTemp<float>(fxTemp<float>, fderivTemp<float>, -5, 10);
    std::cout << "Test newtonTemp with double:" << std::endl;
    newtonTemp<double>(fxTemp<double>, fderivTemp<double>, -5, 10);
    std::cout << "Test newtonTemp with unsigned int:" << std::endl;
    newtonTemp<unsigned int>(fxTemp<unsigned int>, fderivTemp<unsigned int>, -5, 10);
    std::cout << "Test newtonTemp with int:" << std::endl;
    newtonTemp<int>(fxTemp<int>, fderivTemp<int>, -5, 10);
}
