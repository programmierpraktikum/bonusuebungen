#pragma once
#include <cstddef>
#include <functional>
#include <cmath>
#include <iostream>

double newton(std::function<double (double)> fx, std::function<float (float)> fderiv, double x0, size_t n);

template <typename T, typename S>
T newtonTemp(S fx, S fderiv, T x0, size_t n) {
    // TODO
    double xn = x0;
    for (int i = 0; i < n; ++i) {
        xn = xn - (fx(xn) / fderiv(xn));
    }
    std::cout << xn << std::endl;
    return xn;
}

template <typename T>
T fxTemp(T x) {
    return x * x * x + 4 * x * x - 7 * x + 12;
}

template <typename T>
T fderivTemp(T x) {
    return 3 * x * x + 8 * x - 7;
}
