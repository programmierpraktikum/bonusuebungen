#include "board.hpp"
#include "player.hpp"

#include <iostream>

GameStatus asGameStatus(Color color) {
    switch (color) {
    case Color::CROSS:
        return GameStatus::CROSS;
    default:
        return GameStatus::CIRCLE;
    }
}

GameStatus asGameStatus(Field field) {
    switch (field) {
    case Field::CROSS:
        return GameStatus::CROSS;
    case Field::CIRCLE:
        return GameStatus::CIRCLE;
    default:
        std::abort();
    }
}

Field asField(Color color) {
    switch (color) {
    case Color::CROSS:
        return Field::CROSS;
    default:
        return Field::CIRCLE;
    }
}

Color enemyOf(Color color) {
    switch (color) {
    case Color::CROSS:
        return Color::CIRCLE;
    default:
        return Color::CROSS;
    }
}

Board::Board() : fields(std::vector<std::vector<Field>>(3, std::vector<Field>(3, Field::EMPTY))) {}

std::optional<GameStatus> Board::whoWon() const {

//    Check for three colors in the same row or column
    for (int i=0; i<3; i++) {
        if (fields[i][0] == fields[i][1] and fields[i][1] == fields[i][2] and fields[i][0] != Field::EMPTY)
            return {asGameStatus(fields[i][0])};
        if (fields[0][i] == fields[1][i] and fields[1][i] == fields[2][i] and fields[0][i] != Field::EMPTY)
            return {asGameStatus(fields[0][i])};
    }

//    Check for three colors in a diagonal line
    if (fields[0][0] == fields[1][1] and fields[1][1] == fields[2][2] and fields[1][1] != Field::EMPTY)
        return {asGameStatus(fields[1][1])};
    if (fields[0][2] == fields[1][1] and fields[1][1] == fields[2][0] and fields[1][1] != Field::EMPTY)
        return {asGameStatus(fields[1][1])};

//    If no winner was found, check for a tie
    bool tie = true;
    for (const std::vector<Field> &row: fields)
        for (Field field: row)
            if (field == Field::EMPTY)
                tie = false;

    if (tie)
        return {GameStatus::TIE};

//    If there is no tie as well, the game is not finished yet -> return empty optional
    return {};
}

std::ostream& operator<<(std::ostream& os, const Board& board) {
    for (const std::vector<Field> &row: board) {
        for (Field field: row)
            os << "| " << (field == Field::CIRCLE ? "O " : field == Field::CROSS ? "X " : "  ");

        os << "|\n\n";
    }
    return os;
}
