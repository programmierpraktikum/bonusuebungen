#include "human_player.hpp"

#include <iostream>

HumanPlayer::HumanPlayer(Color color) : Player(color) {}

void HumanPlayer::performNextMove(Board& board) {

    bool field_empty = false;
    std::pair<int, int> coordinates;
//    Repeat until an empty field is provided
    do {
        coordinates = getUserInput();

        if (board[coordinates.first][coordinates.second] == Field::EMPTY)
            field_empty = true;
        else
            std::cout << "Field is not empty!" << std::endl;
    } while (!field_empty);

    board[coordinates.first][coordinates.second] = asField(color);
}

std::pair<int, int> HumanPlayer::getUserInput() {
    while (1) {
        std::cout << "Wo wollen Sie " << (color == Color::CROSS ? "Ihr Kreuz" : "Ihren Kreis")
                  << " setzen? (Zählend von 0)\n"
                  << "Eingabeformat: <Zeile> <Spalte>, zum Beispiel '2 0':" << std::endl;

        std::string input;
        getline(std::cin, input);

//        We are expecting a string matching '^[0-2] [0-2]$', which are 3 characters, neither more nor less
        if (input[1] != ' ' or input.length() != 3) {
            std::cout << "Ungültige Eingabe!" << std::endl;
            continue;
        }

        int x, y;
        try {
            x = std::stoi(input.substr(0,1));
            y = std::stoi(input.substr(2,1));
        } catch (const std::exception &e) {
            std::cout << "Ungültige Eingabe!" << std::endl;
            continue;
        }

        return {x, y};
    }
}
