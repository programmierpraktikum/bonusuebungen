#include "game_controller.hpp"

#include <iostream>

void GameController::play(Player& cross, Player& circle) {
//    Main loop
    while (1) {
        board = Board();
        Player* player_at_turn = rand() % 2 == 0 ? &cross : &circle;
//        Game loop
        while (!board.whoWon().has_value()) {
//        Perform player move
            player_at_turn->performNextMove(board);
//        Switch to other player
            player_at_turn = player_at_turn == &cross ? &circle : &cross;
//          Output current board status
            std::cout << board;
        }

        switch (board.whoWon().value()) {
            case GameStatus::CROSS:
                std::cout << "Cross won!" << std::endl;
                break;
            case GameStatus::CIRCLE:
                std::cout << "Circle won!" << std::endl;
                break;
            default:
                std::cout << "It's a tie!" << std::endl;
        }

        std::cout << "Wanna play again? (yes/no)" << std::endl;

//        Check for correct input und break loop it player wants to stop
        std::string play_again;
        bool correct_input;
        do {
            getline(std::cin, play_again);
            correct_input = play_again == "yes" or play_again == "no";

            if (!correct_input)
                std::cout << "Wrong input, please type 'yes' or 'no'" << std::endl;
        } while (!correct_input);

        if (play_again == "no")
            break;
    }
}