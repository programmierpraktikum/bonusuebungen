#include "normal_player.hpp"
#include <cstdlib>

NormalPlayer::NormalPlayer(Color color) : PerfectPlayer(color), RandomPlayer(color), Player(color) {}

void NormalPlayer::performNextMove(Board& board) {
    if (rand() % 2 == 0)
        PerfectPlayer::performNextMove(board);
    else
        RandomPlayer::performNextMove(board);
}
