#include "random_player.hpp"
#include <cstdlib>
#include <iostream>

RandomPlayer::RandomPlayer(Color color) : Player(color) {}

void RandomPlayer::performNextMove(Board& board) {
    int x,y;

//    Just repeat until an empty field is found
    do {
        x = rand() % 3;
        y = rand() % 3;
    } while (board[x][y] != Field::EMPTY);

    board[x][y] = asField(color);
    std::cout << "Bot performed a move!" << std::endl;
}
