#include "normal_player.hpp"
#include "human_player.hpp"
#include "game_controller.hpp"

#include <iostream>
#include <string>

int main() {
    bool valid_input;
    std::string input;
    Player* ptr_human = new HumanPlayer(Color::CROSS);
    Player* ptr_opponent;
    GameController controller;

    do {
        std::cout << "Gegen welchen Computergegner wollen Sie spielen? (r/n/p)" << std::endl;
        getline(std::cin, input);
        if (input.length() != 1 or (input != "r" and input != "n" and input != "p")) {
            std::cout << "Ungültige Eingabe!" << std::endl;
            valid_input = false;
        } else {
            valid_input = true;
        }
    } while (!valid_input);

    if (input == "r")
        ptr_opponent = new RandomPlayer(Color::CIRCLE);
    else if (input == "n")
        ptr_opponent = new NormalPlayer(Color::CIRCLE);
    else
        ptr_opponent = new PerfectPlayer(Color::CIRCLE);

    controller.play(*ptr_human, *ptr_opponent);
    delete ptr_human;
    delete ptr_opponent;
}
