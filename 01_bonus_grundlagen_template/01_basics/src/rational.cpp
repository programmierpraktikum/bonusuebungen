#include <iostream>
#include "rational.hpp"

using std::ostream;
using std::cout;
using std::endl;

Rational::Rational() {
    counter = 1;
    denominator = 2;
    cout << "Rational 1/2 created" << endl;
}

Rational::Rational(int counter, int denominator) {
    this->counter = counter;
    this->denominator = denominator;
    cout << "Rational " << this->counter << '/' << this->denominator << " created" << endl;
}

Rational::Rational(Rational &r) {
    counter = r.counter;
    denominator = r.denominator;
    cout << "Rational " << this->counter << '/' << this->denominator << " copied" << endl;
}

Rational::~Rational() {
    cout << "Destroying Rational" << endl;
}

Rational& Rational::operator+(const Rational &rhs) {
    int new_denominator = this->denominator * rhs.denominator;
    int new_counter = this->counter * rhs.denominator + rhs.counter * this->denominator;

    Rational* res = new Rational (new_counter, new_denominator);

    return *res;
}

Rational& Rational::operator-(const Rational &rhs) {
    int new_denominator = this->denominator * rhs.denominator;
    int new_counter = this->counter * rhs.denominator - rhs.counter * this->denominator;

    Rational* res = new Rational (new_counter, new_denominator);

    return *res;
}

Rational& Rational::operator*(const Rational &rhs) {
    int new_denominator = this->denominator * rhs.denominator;
    int new_counter = this->counter * rhs.counter;

    Rational* res = new Rational (new_counter, new_denominator);

    return *res;
}

Rational& Rational::operator/(const Rational &rhs) {
    int new_denominator = this->denominator * rhs.counter;
    int new_counter = this->counter * rhs.denominator;

    Rational* res = new Rational (new_counter, new_denominator);

    return *res;
}

bool Rational::operator<(const Rational &rhs) {

    return this->counter * rhs.denominator < rhs.counter * this->denominator;
}

ostream& operator<<(ostream& ost, Rational r) {

    ost << r.counter << '/' << r.denominator;

    return ost;
}

Rational simplify(Rational r) {
    Rational res(r);
    if (r.counter == 0) {
        res.denominator = 1;
        return res;
    }

    int min = r.counter < r.denominator ? r.counter : r.denominator;

    for (int i = min; i > 0; i--) {
        if (r.counter % i == 0 && r.denominator % i == 0) {
            res.counter = r.counter / i;
            res.denominator = r.denominator / i;
            break;
        }
    }

    return res;
}