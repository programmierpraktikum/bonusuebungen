#include "rational.hpp"

using namespace std;

int main() {

    int intVal = 42;
    int* pIntVal = &intVal;
    std::cout << "Wert von IntVal " << intVal << std::endl;
    std::cout << "Wert von &IntVal " << &intVal << std::endl;
    std::cout << "Wert von pIntVal " << pIntVal << std::endl;
    std::cout << "Wert von *pIntVal " << *pIntVal << std::endl;
    std::cout << "Wert von &pIntVal " << &pIntVal << std::endl;

    Rational a(1, 4);
    Rational b(1, 2);

    Rational c = b + a;
    Rational d = b - a;
    Rational e = b * a;
    Rational f = b / a;


    cout << c << endl;
    cout << d << endl;
    cout << e << endl;
    cout << f << endl;

    Rational min = c;
    if (d < min) {
        min = d;
    } if (e < min) {
        min = e;
    } if (f < min) {
        min = f;
    }

    cout << min << endl;

    c = simplify(c);
    cout << c << endl;

    d = simplify(d);
    cout << d << endl;

    e = simplify(e);
    cout << e << endl;

    f = simplify(f);
    cout << f << endl;
}