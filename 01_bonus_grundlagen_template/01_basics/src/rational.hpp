#ifndef BASICS_RATIONAL_HPP
#define BASICS_RATIONAL_HPP

#include <iostream>

using std::ostream;

/**
 * @name Rational
 * @brief
 * @author cppp
 */
class Rational {
public:
    int counter;
    int denominator;
    /**
     * @name Rational();
     * @brief Constructor for Rational
     * @author cppp
     */
    Rational();

    /**
     * @name Rational(int counter, int denominator);
     * @brief Parameterized Constructor for Rational
     * @author cppp
     * @param counter sets the counter of the fraction.
     * @param denominator sets the denominator of the fraction.
     */
    Rational(int counter, int denominator);

    /**
     * @name Rational(Rational &r);
     * @brief Copy Constructor for Rational
     * @author cppp
     */
    Rational(Rational &r);

    /**
     * @name ~Rational();
     * @brief Destructor for Rational
     * @author cppp
     */
    ~Rational();

    Rational& operator+(const Rational &rhs);
    Rational& operator-(const Rational &rhs);
    Rational& operator*(const Rational &rhs);
    Rational& operator/(const Rational &rhs);
    bool operator<(const Rational &rhs);
};

ostream& operator<<(ostream &ost, Rational r);
Rational simplify(Rational r);

#endif //BASICS_RATIONAL_HPP