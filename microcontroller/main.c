#include <stdlib.h>
#include "init.h"
#include "acceleration_app.h"
#include "gfx.h"
#include "analog.h"
#include "delay.h"
#include "pins.h"

#define BOARD_COLOR RED
#define SELECT_COLOR WHITE
#define CIRCLE_COLOR GREEN
#define CROSS_COLOR WHITE

#define CROSS_WIDTH 10
#define LINE_WIDTH 10
#define BOARD_START_HORIZONTAL ((WIDTH-HEIGHT)/2.0)
#define BOARD_WIDTH HEIGHT
#define FIELD_WIDTH (BOARD_WIDTH/3.0-LINE_WIDTH)

#define BYTE uint8_t

typedef enum {
    empty,
    circle,
    cross
} Symbol;

typedef struct {
    uint16_t x;
    uint16_t y;
} Pixel;

typedef struct {
  BYTE x;
  BYTE y;
} Field;

typedef enum {false, true} bool;

void draw_field() {

    // Horizontal lines
    cppp_fillRect(BOARD_START_HORIZONTAL, HEIGHT/3.0 - LINE_WIDTH/2.0, BOARD_WIDTH, LINE_WIDTH, BOARD_COLOR);
    cppp_fillRect(BOARD_START_HORIZONTAL, 2*HEIGHT/3.0 - LINE_WIDTH/2.0, BOARD_WIDTH, LINE_WIDTH, BOARD_COLOR);

    // Vertical lines
    cppp_fillRect(BOARD_START_HORIZONTAL + BOARD_WIDTH/3.0 - LINE_WIDTH/2.0, 0, LINE_WIDTH, HEIGHT, BOARD_COLOR);
    cppp_fillRect(BOARD_START_HORIZONTAL + 2*BOARD_WIDTH/3.0 - LINE_WIDTH/2.0, 0, LINE_WIDTH, HEIGHT, BOARD_COLOR);

}

void draw_empty(Pixel p) {

    cppp_fillRect(p.x-FIELD_WIDTH/2.0, p.y-FIELD_WIDTH/2.0, FIELD_WIDTH, FIELD_WIDTH, BLACK);

}

void draw_circle(Pixel p) {

    cppp_fillCircle(p.x, p.y, FIELD_WIDTH/3.0, CIRCLE_COLOR);
    cppp_fillCircle(p.x, p.y, 2*FIELD_WIDTH/9.0, BLACK);

}

void draw_cross(Pixel p) {

    p.x = p.x - CROSS_WIDTH/2;

    int left = p.x-FIELD_WIDTH/3;
    int down = p.y-FIELD_WIDTH/3;
    int right = p.x+FIELD_WIDTH/3;
    int up = p.y+FIELD_WIDTH/3;

    for (int i=0; i<CROSS_WIDTH; i++) {
        cppp_drawLine(left+i, down, right+i, up, CROSS_COLOR);
        cppp_drawLine(left+i, up, right+i, down, CROSS_COLOR);
    }
}

void draw_symbol(Symbol sym, Field f) {

    int center_x, center_y, field_size;

    center_x = BOARD_START_HORIZONTAL + f.x*BOARD_WIDTH/3 + BOARD_WIDTH/6;
    center_y = f.y*HEIGHT/3 + HEIGHT/6;

    Pixel p = {center_x, center_y};

    switch (sym) {
        case circle:
            draw_circle(p);
            break;
        case cross:
            draw_cross(p);
            break;
        default:
            draw_empty(p);
    }

}

void select(Field f) {

    // +2 Damit der Kasten nicht direkt an den Linien ist
    int start_x = BOARD_START_HORIZONTAL + f.x*BOARD_WIDTH/3 + LINE_WIDTH/2 + 2;
    int start_y = f.y*HEIGHT/3 + LINE_WIDTH/2 + 2;

    cppp_drawRect(start_x, start_y, FIELD_WIDTH-4, FIELD_WIDTH-4, SELECT_COLOR);
}

void deselect(Field f) {

    // +2 Damit der Kasten nicht direkt an den Linien ist
    int start_x = BOARD_START_HORIZONTAL + f.x*BOARD_WIDTH/3 + LINE_WIDTH/2 + 2;
    int start_y = f.y*HEIGHT/3 + LINE_WIDTH/2 + 2;

    cppp_drawRect(start_x, start_y, FIELD_WIDTH-4, FIELD_WIDTH-4, BLACK);
}

bool had_break = true;
bool get_selected(Field* ptr) {

    if (cppp_readTouchZ() < 128) {
        had_break = true;
        return 0;
    } else if (!had_break) {
        return 0;
    }

    had_break = false;

    int x = cppp_readTouchX();
    int y = cppp_readTouchY();
  
    BYTE field_x = (x - BOARD_START_HORIZONTAL - LINE_WIDTH/2) / (BOARD_WIDTH/3);
    BYTE field_y = (y - LINE_WIDTH/2) / (BOARD_WIDTH/3);

    *ptr = (Field){field_x, field_y};
    return 1;
}

Symbol get_winner(Symbol board[3][3]) {

    for (int i=0; i<3; i++) {
        // Check hotizontal
        if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][1] != empty)
            return board[i][1];
        // Check vertical
        if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[1][i] != empty)
            return board[1][i];
    }

    // Check diagonal
    if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != empty)
        return board[1][1];
    if (board[0][2] == board [1][1] && board[1][1] == board[2][0] && board[1][1] != empty)
        return board[1][1];

    return empty;
}

void translateToGrid(uint8_t joyLeftX, uint8_t joyLeftY, Field* selected) {
    uint8_t threshold = 50;
    if (joyLeftX > 190 + threshold && joyLeftY < 190 + threshold && joyLeftY > 190 - threshold)
    {
        deselect(*selected);
        selected->x = (selected->x-1+3) % 3;
    }
    else if (joyLeftX < 190 - threshold && joyLeftY < 190 + threshold && joyLeftY > 190 - threshold)
    {
        deselect(*selected);
        selected->x = (selected->x+1+3) % 3;
    }
    else if (joyLeftY > 190 + threshold && joyLeftX < 190 + threshold && joyLeftX > 190 - threshold)
    {
        deselect(*selected);
        selected->y = (selected->y-1+3) % 3;
    }
    else if (joyLeftY < 190 - threshold && joyLeftX < 190 + threshold && joyLeftX > 190 - threshold)
    {
        deselect(*selected);
        selected->y = (selected->y+1+3) % 3;
    } else {
        return;
    }
    select(*selected);
    cppp_microDelay(100000);
}

int main() {
    initBoard();
    USER_BUTTON_IO = 0;
    draw_field();

    Symbol turn;
    Symbol board[3][3] = {0};
    Field selected;

    if (rand()%2) turn = circle;

    while(1) {
        if (get_winner(board) == circle) {
            Symbol x = get_winner(board);
            cppp_fillRoundRect(0,0,50,50,5,CIRCLE_COLOR);
            while(1) {}
        } else if (get_winner(board) == cross) {
            cppp_fillRoundRect(0,0,50,50,5,CROSS_COLOR);
            while(1) {}
        }

        uint8_t touchX;
        uint8_t touchY;
        uint8_t joyRightX;
        uint8_t joyLeftX; 
        uint8_t light;
        uint8_t joyLeftY;
        uint8_t joyRightY;

        cppp_getAnalogValues(&touchX, &touchY, &joyRightX, &joyLeftX, &light, &joyLeftY, &joyRightY);
        translateToGrid(joyLeftX, joyLeftY, &selected);

        if(USER_BUTTON_DATA == 0) {
            if (board[selected.x][selected.y] == empty) {
                board[selected.x][selected.y] = turn;
                draw_symbol(turn, selected);
                if (turn == cross) turn = circle;
                else turn = cross;
            }
        }

        Field new_select;
        // Wenn nichts ausgewÃ¤hlt wird, passiert nichts
        if (!get_selected(&new_select)) continue;

        if (selected.x == new_select.x && selected.y == new_select.y) {
            if (board[selected.x][selected.y] == empty) {
                board[selected.x][selected.y] = turn;
                draw_symbol(turn, (selected));

                if (turn == cross) turn = circle;
                else turn = cross;
            }

            deselect(selected);
        } else {
            deselect(selected);
            select(new_select);
            selected = new_select;
        }
    }

    return 0;
}
