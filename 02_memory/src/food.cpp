#include "food.hpp"
#include <fstream>
#include <sstream>
#include <math.h>
#include <stdexcept>

#include <iostream>

using namespace std;

Food::Food(int bestellnummer, string bezeichnung, float preis) : bestellnr(bestellnummer), bezeichnung(bezeichnung), preis(preis) {}

string Food::getBezeichnung() const {
    return bezeichnung;
}

float Food::getPreis() const {
    return preis;
}

int Food::getBestellnummer() const {
    return bestellnr;
}

int parse_integer(const char* input) {
    int res = 0;
    for (int i = 0; input[i] != '\0'; i++) {

        if (input[i] < '0' || input[i] > '9') {
            throw runtime_error("Expected numerical characters to convert to integer, got something different");
        }

        res *= 10;
        res += input[i] - '0';
    }
    return res;
}

float parse_float(const char* input) {
    float res = 0.0;
    int j = 0;
    for (int i = 0; input[i] != '.'; i++) {

        if (input[i] < '0' || input[i] > '9') {
            throw runtime_error("Expected numerical characters to convert to float, got something different");
        }

        res *= 10;
        res += input[i] - '0';
        j++;
    }
    int count = 0;
    for (int i = j + 1; input[i] != '\0'; i++) {

        if (input[i] < '0' || input[i] > '9') {
            throw runtime_error("Expected numerical characters in float, got something different");
        }

        res += (input[i] - '0') * pow(10, -(count+1));
        count++;
    }

    return res;
}

void speichern(const string& dateiname, const vector<Food>& speisen) {
    fstream stream;
    stream.open(dateiname, ios_base::out);
    for (int i = 0; i < speisen.size(); i++) {
        stream << speisen[i].getBestellnummer() << ';' << speisen[i].getBezeichnung() << ';' << speisen[i].getPreis() << endl;
    }
    stream.close();
}

void laden(const string& dateiname, vector<Food>& speisen) {
    fstream stream;
    stream.open(dateiname, ios_base::in);
    speisen.clear();

    while (stream.good()) {

        char* bestellnummer = new char[100];
        char* bezeichnung = new char[100];
        char* preis = new char[100];

        stream.getline(bestellnummer, 100, ';');
        stream.getline(bezeichnung, 100, ';');
        stream.getline(preis, 100, '\n');

        if (!stream.eof()) {
            
            if (stream.fail()) {
                throw runtime_error("Failbt is set, a csv entry seems to be >=99 characters long. Please use shorter entries");
            } else if (stream.bad()) {
                throw runtime_error("Badbit is set on csv input stream, may have encountered a reading error on one of the last 3 reads");
            }

            Food entry(parse_integer(bestellnummer), bezeichnung, parse_float(preis));
            speisen.insert(speisen.end(), entry);
        }

        delete[] bestellnummer;
        delete[] bezeichnung;
        delete[] preis;
    }
    stream.close();
}

