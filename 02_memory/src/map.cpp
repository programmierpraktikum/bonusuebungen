#include "map.hpp"
#include <string>
#include <vector>
#include <stdexcept>
#include <algorithm>
#include <iostream>

cppp::Map::Map(const std::size_t size) : size(size) {
    // TODO
    arr = new MapBucket[size];
}

cppp::Map::~Map() {
    // TODO
    delete[] arr;
}

void cppp::Map::insert(const std::string& key, const std::vector<Item>& order) {
    // TODO
    int index = calcHash(key);
    MapElement ele = {key, order};

    if (arr[index].empty()) {
        arr[index] = { ele };
    }

    bool vorkommen = false;
    for(int i = 0; i < arr[index].size(); i++) {
        if(arr[index].at(i).key == key) {
            vorkommen = true;
            arr[index].at(i) = ele;
        }
    }
    if(!vorkommen) {
        arr[index].push_back(ele);
    }

}

std::vector<cppp::Item> cppp::Map::get(const std::string& key) {
    // TODO
    std::vector<cppp::Item> result;
    int index = calcHash(key);
    bool vorgekommen = false;
    for(int i = 0; i < arr[index].size(); i++) {
        if(arr[index].at(i).key == key) {
            vorgekommen = true;
             result = arr[index].at(i).value;
        }
    }
    if(!vorgekommen) {
        std::string err = "Das Element wurde nicht gefunden!";
        throw std::invalid_argument(err);
    }
     return result;
}

void cppp::Map::remove(const std::string& key) {
    // TODO
    int index = calcHash(key);
    for(int i = 0; i < arr[index].size(); i++) {
        if(arr[index].at(i).key == key) {
            std::vector<MapElement> temp;
            for(int j = 0; j < arr[index].size(); j++) {
                if(j != i) {
                    temp.push_back(arr[index].at(j));
                }
            }
                arr[index] = temp;
        }
    }
}

std::size_t cppp::Map::calcHash(const std::string& key) {
    // TODO
    int result = 0;
    for(int i = 0; i < key.size(); i++) {
        result += key[i];
    }
    return result % size;
}
